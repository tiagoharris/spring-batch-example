package com.tiago.model;

import java.time.LocalDate;

/**
 * Model class for table "User" 
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class User {
  
  private Integer id;
  
  private String name;

  private String email;
  
  private String phoneNumber;
  
  private LocalDate birthDate;
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }
}
