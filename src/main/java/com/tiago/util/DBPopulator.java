package com.tiago.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Utility class to generate a CSV file used to initialize a table.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class DBPopulator {
  
  private static int MAX_WORKERS = 8;
  
  private static AtomicInteger COUNT = new AtomicInteger(0);
  
  private static final int AMOUNT = 10_000_000;
  
  private static final String CSV_LINE = "%1$s,\"name %1$s\",\"email@email.com\",\"99999999\",\"1984-01-01\"\n";

  private static final String FILE_NAME = "inserts.csv";
  
  public static void main(String[] args) throws IOException {
    
    System.out.println("Writing file " + FILE_NAME);
    
    BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME));
    
    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(MAX_WORKERS);

    for (int i = 0; i < AMOUNT; i++) {
      executor.submit(() -> {
        writer.write(String.format(CSV_LINE, COUNT.incrementAndGet()));
        return null;
      });
    }
    
    executor.shutdown();

    try {
      if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
        executor.shutdownNow();
        if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
          System.err.println("Pool did not terminate");
        }
      }
    } catch (InterruptedException e) {
      executor.shutdownNow();
    } finally {
      writer.close();
      System.out.println("Finished. " + COUNT.get() + " insert statements were generated.");
    }
  }
}
