package com.tiago.batch.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.tiago.model.User;

/**
 * Maps a row into a {@link User} object.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class UserRowMapper implements RowMapper<User> {

  /* (non-Javadoc)
   * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
   */
  @Override
  public User mapRow(ResultSet rs, int rowNum) throws SQLException {
    User user = new User();
    user.setId(rs.getInt("id"));
    user.setName(rs.getString("name"));
    user.setEmail(rs.getString("email"));
    user.setPhoneNumber(rs.getString("phone_number"));
    user.setBirthDate(rs.getDate("birth_date").toLocalDate());

    return user;
  }
}
