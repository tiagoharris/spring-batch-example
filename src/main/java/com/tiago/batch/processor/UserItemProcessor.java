package com.tiago.batch.processor;

import org.springframework.batch.item.ItemProcessor;

import com.tiago.model.User;

/**
 * Process the {@link User} object, applying some transformation.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public class UserItemProcessor implements ItemProcessor<User, User> {

  /* (non-Javadoc)
   * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
   */
  @Override
  public User process(User user) throws Exception {
    user.setName(user.getName().toUpperCase());
    user.setEmail(user.getEmail().toUpperCase());
    
    return user;
  }

}