package com.tiago.configuration;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import com.tiago.batch.processor.UserItemProcessor;
import com.tiago.batch.rowmapper.UserRowMapper;
import com.tiago.model.User;

/**
 * Configuration class for batch processing.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  @Autowired
  public StepBuilderFactory stepBuilderFactory;

  @Autowired
  public DataSource dataSource;

  private static final int CHUNK_SIZE = 10_000;

  @Bean
  public JdbcCursorItemReader<User> reader() {
    JdbcCursorItemReader<User> reader = new JdbcCursorItemReader<User>();
    reader.setDataSource(dataSource);
    
    reader.setFetchSize(Integer.MIN_VALUE);
    reader.setVerifyCursorPosition(false);
    
    reader.setSql("SELECT id,name,email,phone_number,birth_date FROM user");
    reader.setRowMapper(new UserRowMapper());

    return reader;
  }

  @Bean
  public FlatFileItemWriter<User> writer() {
    FlatFileItemWriter<User> writer = new FlatFileItemWriter<User>();
    writer.setResource(new FileSystemResource("users.csv"));
    writer.setLineAggregator(new DelimitedLineAggregator<User>() {
      {
        setDelimiter(",");
        setFieldExtractor(new BeanWrapperFieldExtractor<User>() {
          {
            setNames(new String[] { "id", "name", "email", "phoneNumber", "birthDate" });
          }
        });
      }
    });

    return writer;
  }

  @Bean
  public UserItemProcessor processor() {
    return new UserItemProcessor();
  }

  @Bean
  public Step step1() {
    return stepBuilderFactory.get("step1").<User, User>chunk(CHUNK_SIZE)
        .reader(reader())
        .processor(processor())
        .writer(writer())
        .build();
  }

  @Bean
  public Job exportUserJob() {
    return jobBuilderFactory.get("exportUserJob")
        .incrementer(new RunIdIncrementer())
        .flow(step1())
        .end()
        .build();
  }
}
