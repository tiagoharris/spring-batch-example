# spring-batch-example

A demo project to show Spring Batch integration with Spring Boot.

## Stack:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Spring Batch](https://spring.io/projects/spring-batch)
* [Maven](https://maven.apache.org/)


